// SPDX-FileCopyrightText: 2024 Thomas Kramer
// SPDX-License-Identifier: AGPL-3.0-or-later

use criterion::{black_box, criterion_group, criterion_main, Criterion};

use iron_shapes::prelude::{traits::*, MultiPolygon, Polygon, Vector};
use iron_shapes_booleanop::BooleanOp;
use itertools::Itertools;

/// Benchmark intersection and union of two rectangles.
fn bench_two_rectangles(c: &mut Criterion) {
    let p1 = Polygon::from(vec![(0., 0.), (2., 0.), (2., 2.), (0., 2.)]);
    let p2 = p1.translate((1., 1.).into());

    c.bench_function("intersection", |b| {
        b.iter(|| {
            //
            let _i = p1.intersection(black_box(&p2));
        })
    });

    c.bench_function("union", |b| {
        b.iter(|| {
            //
            let _u = p1.union(black_box(&p2));
        })
    });
}

/// Benchmark intersection and union of two sets of each 1000 rectangles.
fn bench_1k_rectangles(c: &mut Criterion) {
    let p1 = Polygon::from(vec![(0., 0.), (2., 0.), (2., 2.), (0., 2.)]);
    // Create 1000 rectangles on a grid.
    let mp1 = MultiPolygon::from_polygons(
        (0..50)
            .cartesian_product(0..20)
            .map(|(x, y)| {
                let position = Vector::new(x as f64, y as f64) * 4.;
                p1.translate(position)
            })
            .collect(),
    );
    let mp2 = mp1.translate((1., 1.).into());

    c.bench_function("intersection", |b| {
        b.iter(|| {
            //
            let _i = mp1.intersection(&mp2);
        })
    });

    c.bench_function("union", |b| {
        b.iter(|| {
            //
            let _u = mp1.union(&mp2);
        })
    });
}

criterion_group!(benches, bench_two_rectangles, bench_1k_rectangles);
criterion_main!(benches);
